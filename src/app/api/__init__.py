from .controller import bp


class API:
    def __init__(self, flask_app) -> None:
        flask_app.register_blueprint(bp)
