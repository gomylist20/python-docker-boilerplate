from flask import Blueprint

bp = Blueprint('api', __name__, url_prefix='/api/v1')


@bp.route("/test")
def test():
    return "I'm THE API!"
