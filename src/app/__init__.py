from flask import Flask
from flask_cors import CORS
from .api import API

app = Flask(__name__)
CORS(app)

# Register api in falsk
API(app)


@app.route("/")
def hello():
    return "Hello World!"
